# Halo 3 Custom Edition UI

Possible replacement for Halo Online's UI heavily based on Halo: Reach. 

&nbsp;

#### License:
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br/>Licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

&nbsp;

#### Find Me:

* Reddit:  <a href="http://reddit.com/user/GiveMeBTC">/u/GiveMeBTC</a> and <a href="http://reddit.com/user/thefeeltrain">/u/thefeeltrain</a>
* Website:  <a href="http://eriq.co">eriq.co</a>
* Youtube: <a href="https://www.youtube.com/channel/UCKtnHACO0eB0cIlK9FlQZ5w">Brayden Strasen</a>

&nbsp;

#### Credits:
* The <a href="https://github.com/FishPhd/DewritoLauncher">ElDewrito</a> developers for being awesome
* Microsoft, 343 Industries, Bungie
* <a href="http://reddit.com/user/Shadowfita">Shadowfita</a> for getting the server browser working
* <a href="http://reddit.com/user/RackJonan">/u/RackJonan</a> for the Halo CE background
* <a href="http://reddit.com/user/rexadde">/u/rexadde</a> for the logo
* Stephan Swinford for the "Crash" background
